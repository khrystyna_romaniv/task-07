package com.epam.rd.java.basic.task7.db.entity;

public class User {

	private int id;

	private String login;

	public User(String login) {
		setLogin(login);
		setId(0);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLogin() {
		return this.login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public static User createUser(String login) {

		return new User(login);
	}

	@Override public String toString(){
		return this.getLogin();
	}

	@Override public boolean equals(Object obj){
		return obj.toString().equals(this.getLogin());
	}

}
