package com.epam.rd.java.basic.task7.db.entity;

public class Team {

	private int id;

	private String name;

	public Team(String name) {
		setName(name);
		setId(0);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static Team createTeam(String name) {
		return new Team(name);
	}

	@Override public String toString(){
		return this.getName();
	}

	@Override public boolean equals(Object obj){
		return obj.toString().equals(this.getName());
	}

}
