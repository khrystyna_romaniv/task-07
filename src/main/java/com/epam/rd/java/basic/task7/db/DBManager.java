package com.epam.rd.java.basic.task7.db;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

	private static DBManager instance;
	private Connection connection;

	public static synchronized DBManager getInstance() {

		return new DBManager();
	}

	private DBManager() {
		try {
			connection = DriverManager.getConnection("jdbc:derby:memory:testdb;create=true");
			//connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/test2db","user","user");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public List<User> findAllUsers() throws DBException {
		List<User> users = new ArrayList<>();
		User user;
		try {
			Statement statement = connection.createStatement();
			String query = "SELECT * FROM users";
			ResultSet resultSet = statement.executeQuery(query);
			while (resultSet.next()){
				user = new User(resultSet.getString("login"));
				user.setId(Integer.parseInt(resultSet.getString("id")));
				users.add(user);
			}
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return users;
	}

	public boolean insertUser(User user) throws DBException {
		PreparedStatement statement = null;
		try {
			statement = (PreparedStatement) connection.prepareStatement("INSERT INTO users VALUES (DEFAULT, ?)");
			statement.setString(1, user.getLogin());
			statement.executeUpdate();
			statement = connection.prepareStatement("SELECT * FROM users WHERE login = ?");
			statement.setString(1,user.getLogin());
			ResultSet resultSet = statement.executeQuery();
			resultSet.next();
			user.setId(Integer.parseInt(resultSet.getString("id")));
			resultSet.close();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean deleteUsers(User... users) throws DBException {
		PreparedStatement statement = null;
		try {
			statement = (PreparedStatement) connection.prepareStatement("DELETE FROM users WHERE login=?");
			for(int i=0;i<users.length;i++){
				statement.setString(1, users[i].getLogin());
				statement.executeUpdate();
			}
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	public User getUser(String login) throws DBException {
		User user = new User(login);
		try {
			PreparedStatement statement = (PreparedStatement) connection.prepareStatement("SELECT * FROM users WHERE login = ?");
			statement.setString(1,login);
			ResultSet resultSet = statement.executeQuery();
			resultSet.next();
			user.setId(Integer.parseInt(resultSet.getString("id")));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return user;
	}

	public Team getTeam(String name) throws DBException {
		Team team = new Team(name);
		try {
			PreparedStatement statement = (PreparedStatement) connection.prepareStatement("SELECT * FROM teams WHERE name = ?");
			statement.setString(1,name);
			ResultSet resultSet = statement.executeQuery();
			resultSet.next();
			team.setId(Integer.parseInt(resultSet.getString("id")));
			resultSet.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return team;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> teams = new ArrayList<>();

		try {
			Statement statement = null;
			statement = connection.createStatement();
			String query = "SELECT * FROM teams";
			ResultSet resultSet = statement.executeQuery(query);
			while (resultSet.next()){
				teams.add(new Team(resultSet.getString("name")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return teams;
	}

	public boolean insertTeam(Team team) throws DBException {
		PreparedStatement statement = null;
		try {
			statement = (PreparedStatement) connection.prepareStatement("INSERT INTO teams VALUES (DEFAULT, ?)");
			statement.setString(1, team.getName());
			statement.executeUpdate();

			statement = connection.prepareStatement("SELECT * FROM teams WHERE name = ?");
			statement.setString(1,team.getName());
			ResultSet resultSet = statement.executeQuery();
			resultSet.next();
			team.setId(Integer.parseInt(resultSet.getString("id")));
			resultSet.close();
			return true;

		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		PreparedStatement statement = null;
		List<Team> list = getUserTeams(user);
		for(Team team: teams){
			for(int i=0;i<list.size();i++){
				if(team.getName().equals(list.get(i).getName())) throw new DBException("Thrown exception musb be a DBException", new Throwable());
			}
		}
		try {
			statement = (PreparedStatement) connection.prepareStatement("INSERT INTO users_teams VALUES (?, ?)");
			int id=0;
			for(int i=0;i<teams.length;i++){
				String name = teams[i].getName();
				PreparedStatement st = connection.prepareStatement("SELECT id FROM teams WHERE name=?");
				st.setString(1,name);
				ResultSet resultSet = st.executeQuery();
				resultSet.next();
				id=Integer.parseInt(resultSet.getString("id"));
				statement.setInt(1, user.getId());
				statement.setInt(2, id);
				statement.addBatch();
			}
			statement.executeBatch();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> teams = new ArrayList<>();
		try {
			PreparedStatement statement = (PreparedStatement) connection.prepareStatement("SELECT * FROM users_teams INNER JOIN users ON users_teams.user_id=users.id WHERE login=?");
			statement.setString(1, user.getLogin());
			ResultSet resultSet = statement.executeQuery();
			while (resultSet.next()){
				int id = Integer.parseInt(resultSet.getString("team_id"));
				PreparedStatement statement1 = (PreparedStatement) connection.prepareStatement("SELECT * FROM teams WHERE id=?");
				statement1.setInt(1, id);
				ResultSet resultSet1 = statement1.executeQuery();
				resultSet1.next();
				teams.add(new Team(resultSet1.getString("name")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return teams;
	}

	public boolean deleteTeam(Team team) throws DBException {
		PreparedStatement statement = null;
		try {
			statement = (PreparedStatement) connection.prepareStatement("DELETE FROM teams WHERE name=?");
			statement.setString(1, team.getName());
			statement.executeUpdate();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

	public boolean updateTeam(Team team) throws DBException {
		PreparedStatement statement = null;
		try {
			statement = (PreparedStatement) connection.prepareStatement("UPDATE teams SET name = ? WHERE id=?");
			statement.setString(1, team.getName());
			statement.setInt(2,team.getId());
			statement.executeUpdate();
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
	}

}
